# Generated by Django 3.0.2 on 2020-02-04 05:59

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('first_app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='blog',
            old_name='name',
            new_name='title',
        ),
        migrations.AddField(
            model_name='blog',
            name='body',
            field=models.TextField(default='default body....'),
        ),
        migrations.AddField(
            model_name='blog',
            name='modified_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2020, 2, 4, 5, 59, 54, 557631, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='blog',
            name='published_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
