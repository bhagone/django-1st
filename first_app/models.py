from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.

# class Author(models.Model):
#     name = models.CharField(max_length=100)

class Blog(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(default='default body....')
    published_date = models.DateTimeField(auto_now=True)
    modified_date = models.DateTimeField(auto_now_add=True)
    # author = models.OneToOneField(Author, on_delete='CASCADE')

    def __str__(self):
        return self.title

