from django.shortcuts import render,redirect

# Create your views here.
from django.http import HttpResponse

from .models import Blog
from .forms import BlogForm

def all_news(request):
    data = Blog.objects.all()
    context = {'news':data}
    return render(request, 'all_news.html', context)

def sample_title(request):
    news = Blog.objects.filter(title = 'sample')
    context =  {'news':news}
    return render(request, 'sample_title.html', context)

def about(request):
    return render(request, 'about.html')


def create(request):
    form = BlogForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('allnews')

    context = {
        'form':form
    }
    
    return render(request, 'create.html', context)

def remove(request, id):
    a = Blog.objects.get(pk=id)
    a.delete()
    return redirect('allnews')

def edit(request, id):
    b = Blog.objects.get(pk=id)

    form = BlogForm(request.POST or None, instance=b)
    if form.is_valid():
        form.save()
        return redirect('allnews')
    context = {
        'form': form,
    }
    return render(request, 'create.html', context)

