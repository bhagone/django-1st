from django.urls import path

from first_app import views


urlpatterns = [
    # path('index', views.index),
    path('all', views.all_news,name='allnews'),
    path('about', views.about,name='about'),
    path('create', views.create,name='create'),
    path('sample', views.sample_title,name='sample_title'),

    
    path('delete/<int:id>',views.remove,name='del'),
    path('edit/<int:id>',views.edit,name='edit'),
    # path('ob', views.return_obj)
]